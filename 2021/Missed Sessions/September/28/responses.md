
>>>
Check the Powerpoint File for comparison or reference if you perfer.
>>>

# Slide 4

##### Don’t share any names, but have you ever wished someone in your life would change?

Absolutely. I wishes that we would apply more skills together and get more along better as planned. That way things improve like doing the whip and nae nae like a champ.

##### How did your desire and attempts to change that person affect your relationship with him or her?

When I tried... I failed miserably. My mind was so out of whack, I couldn't think of what to do, and it was to the point where my brain was like in the picture below.

![https://images.fineartamerica.com/images-medium-large-5/face-of-a-man-screaming-in-pain-or-rage-sheila-terryscience-photo-library.jpg]

That is what it felt like inside. Anger, frustration... and all...

# Slide 8

##### Think of that person in your life you wrote about earlier who you wish you could change. How would it feel to ask The Ultimate QBQ! regarding your desire to change this person?

This, is a good one to think of. I'll include all I can think of, on 3 responses. My brain's going to get slapped from a big fat oak tree log for this one.

| # | Lousy Question | The Ultimate QBQ! |
| ----- | ----- | ----- |
| 1 | Why is it sssooooooooo haaaarrddd????? | How is it making it hard? Is there something the cause? |
| 2 | This is a pain in the face. Why can't I get a fair response? | How do I get a fair response? |
| 3 | Darn. Why isn't it getting applied correctly? | How do I apply it correctly? |

Yep. Got slapped. But I went through all the possible ones.



























